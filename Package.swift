// swift-tools-version: 5.8
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "KecilinMediaCompressor",
    platforms: [
      .macOS(.v12), .iOS(.v15)
    ],
    products: [
        .library(
            name: "KecilinMediaCompressor",
            targets: ["KecilinMediaCompressor"]),
    ],
    dependencies: [
    ],
    targets: [
        .binaryTarget(
            name: "KecilinMediaCompressor",
            path: "./KecilinMediaCompressor.xcframework")
    ]
)
